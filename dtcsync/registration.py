# -*- coding: utf-8 -*-

from decimal import *
import datetime

global_cions = {}
global_sums = {}
global_counts = {}


def register ( amount, d, cion ):
    date = str(d)[:10]
    #print "Registering %s -> %f" % ( d, amount )

    if cion:
        cion_amount = round(Decimal("%.02f" % amount)*Decimal('0.009') + Decimal('0.10'), 2)
        if not date in global_cions:
            global_cions [ date ] = Decimal ( 0 )
        global_cions [ date ] += Decimal ( "%.02f" % cion_amount )
        #print "%s: %.2f, adding cion %.2f" % ( date, amount, cion_amount )
    
    if not date in global_sums:
        global_sums [ date ] = Decimal ( 0 )
    global_sums [ date ] += Decimal ( "%.02f" % amount )


def handle_registrations ( doc ):
    tpe = doc.find_account ( u'TPE immédiat' )
    tpe_payee = doc.find_payee ( u'TPE immédiat' )
    cion_payee = doc.find_payee ( u'TPE CION immédiat' )
    ( c, s ) = doc.find_category_sub_category ( '6680' )

    print """<h2>Importing TPE transfers and fees</h2>\n
    <table class="supertable">
    <tr><th>Day</th><th>Sum</th><th>Commission</th></tr>
    """

    cm = doc.find_account ( u'Crédit Mutuel' )
    for day in sorted(global_sums):
        print "<tr><td>%s</td><td>%.02f&euro;</td>"% ( day, global_sums [ day ] )

        if day in global_cions:
            cion = cm.create_transaction ( cion_payee )
            cion.set_payment_mode ( 19 )
            cion.set_date ( day )
            cion.set_category ( c, s )
            cion.set_amount ( - global_cions [ day ] )
            cion.set_financial_year_by_date ( day )
            cion.set_comment ( u'Frais bancaires pour TPE (saisie auto)' )
            print "<td>%.02f&euro;</td>" % - global_cions [ day ]
        else:
            print "<td>N/A</td>"

        t1 = cm.create_transaction ( tpe_payee )
        t1.set_payment_mode ( 19 )
        t1.set_date ( day )
        t1.set_amount ( global_sums [ day ] )
        t1.set_financial_year_by_date ( day )
        t1.set_comment ( u'Encaissement journalier (saisie auto)' )

        t2 = tpe.create_transaction ( tpe_payee )
        t2.set_payment_mode ( 31 )
        t2.set_date ( day )
        t2.set_amount ( - global_sums [ day ] )
        t2.set_financial_year_by_date ( day )
        t2.set_comment ( u'Encaissement journalier (saisie auto)' )
        
        t1.link_to_transaction ( t2 )

        print "</tr>"

    print "</table>"

