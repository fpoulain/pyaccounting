# -*- coding: utf-8 -*-

import sys, re

sys.path.append('../../pydtc/pyDTC')
from pyDTC import *

sys.path.append('../pyaccounting')
from GrisbiDocument import *

def import_transfers ( doc ):
    dtc=pyDTC('mysql://','pydtc.conf')
    cm = doc.find_account ( u'Crédit Mutuel' )

    start_date = '-'.join ( re.match ( 'Sync auto (\d\d)/(\d\d)/(\d\d\d\d)', cm.get_comment ( ) ).group(3,2,1) )
    print """<h2>Importing transfers from %s</h2>\n
    <table class="supertable">
    <tr><th>Day</th><th>Category</th><th>Date</th><th>Amount</th></tr>
    """ % start_date

    for e in dtc.query(Subscription).\
            filter ( Subscription.payment_received_date != None ).\
            filter ( Subscription.payment_received_date >= start_date ).\
            filter ( Subscription.payment_received_date < func.date(func.now()) ).\
            filter ( Subscription.payment_mode == 'virement' ):
        if e.membership.actor.actor_type == 'person':
            name = e.membership.actor.person.fullname()
        else:
            name = e.membership.actor.entity.fullname()

        payee = doc.find_payee ( name )
        if not payee:
            payee = doc.create_payee ( name )
            new = ' <span style="padding:0px 0.5em;background:#f00;color:#fff;">CREATED</span>'
        else:
            new = ''
        print "<tr><td><b>%s%s</b></td>" % ( name, new )

        found = doc.find_transactions ( { 'date': e.payment_received_date,
                                          'account': cm,
                                          'payee': payee } )

        if not found:
#            print "Found no transfer transaction for %s, creating" % name
            t = cm . create_transaction ( payee )
            ( c, s ) = doc.find_category_sub_category ( '7560' )
            print "<td>%s</td><td>%s</td><td>%.02f&euro;</td></tr>" % ( s.get_name(), e.payment_received_date.strftime('%d/%m/%Y'), e.amount )
            t.set_payment_mode ( 13 )
            t.set_category ( c, s )
            t.set_amount ( e.amount )
            t.set_date ( e.payment_received_date )
            t.set_financial_year_by_date ( e.payment_received_date )
            if e.reference:
                t.set_comment ( u"Adhésion personne physique (saisie auto, %s)" % e.reference )
            else:
                t.set_comment ( u"Adhésion personne physique (saisie auto)" )
        else:
            print "<td colspan=\"3\"><b>Found existing !</b></td></tr>"
#            print "Found %d transfer transaction(s) for %s" % ( found, name )

    print "</table>"

    # SQLalchemy cleanup
    clear_mappers()
