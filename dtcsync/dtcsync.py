#!/usr/bin/python
# -*- coding: utf-8 -*-
 

from dtc import *
from recur import *
from thelia import *
from transfers import *
from registration import *

import sys, codecs, locale, datetime
sys.path.append('../pyaccounting/')
from GrisbiDocument import *

sys.stdout = codecs.getwriter('UTF-8')(sys.stdout)

if len(sys.argv) != 2:
    print "Usage: dtcsync.py <file>"
    sys.exit ( 1 )

try:
    doc = GrisbiDocument ( sys.argv [ 1 ] )
except Exception as e:
    print "Error:", e
    sys.exit ( 1 )

try:
    import_thelia ( doc )
    import_cbweb ( doc )
    import_recur ( doc )
    import_transfers ( doc )
    handle_registrations ( doc )

    cm = doc.find_account ( u'Crédit Mutuel' )
    cm.set_comment ( 'Sync auto %s' % date.today().strftime('%d/%m/%Y') )

    doc.serialize ( sys.argv [ 1 ] )

except Exception as e:
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback_details = {
        'filename': exc_traceback.tb_frame.f_code.co_filename,
        'lineno'  : exc_traceback.tb_lineno,
        'name'    : exc_traceback.tb_frame.f_code.co_name,
        'type'    : exc_type.__name__,
        'message' : exc_value.message, # or see traceback._some_str()
        }
    traceback_template = '''File "%(filename)s", line %(lineno)s, in %(name)s
%(type)s: %(message)s\n''' # Skipping the "actual line" item

    print "<p style=\"font-size: 200%%;color:red;\">Error: %s</p>" % ( traceback_template % traceback_details )

sys.exit(0)
