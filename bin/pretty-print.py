#!/usr/bin/python
# -*- coding: utf-8 -*-
 
import sys
from lxml import etree

sys.path.append('pyaccounting')
from GrisbiDocument import *


if len(sys.argv) != 2:
    print "Usage: dtcsync.py <file>"
    sys.exit ( 1 )

try:
    doc = GrisbiDocument ( sys.argv [ 1 ] )
except Exception as e:
    print "Error:", e
    sys.exit ( 1 )

cm = doc.find_account ( u'Crédit mutuel' )
payee = doc.create_payee ( u'Matt Murdock' )
t = cm . create_transaction ( payee )
( c, s ) = doc.find_category_sub_category ( '7560' )
t.set_category ( c, s )
t.set_amount ( 42 )

doc.serialize ( 'out2.gsb' )

sys.exit(0)
