#!/usr/bin/python
# -*- coding: utf-8 -*-
 
import sys
from lxml import etree

sys.path.append('pyaccounting')
from GrisbiDocument import *

if len(sys.argv) != 2:
    print "Usage: fix-recur-transfers.py <file>"
    sys.exit ( 1 )

try:
    doc = GrisbiDocument ( sys.argv [ 1 ] )
except Exception as e:
    print "Error:", e
    sys.exit ( 1 )

payee = doc.find_payee ( 'Prelevement par CB', True )
for t in payee.get_transactions ( { 'date': '2013-04-17' } ):
    print "%s: %s" % ( t.get_date ( ), t.get_amount ( ) )
