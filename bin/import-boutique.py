#!/usr/bin/python
# -*- coding: utf-8 -*-
 
import sys
from lxml import etree

sys.path.append('pyaccounting')
from GrisbiDocument import *


if len(sys.argv) != 2:
    print "Usage: dtcsync.py <file>"
    sys.exit ( 1 )

try:
    doc = GrisbiDocument ( sys.argv [ 1 ] )
except Exception as e:
    print "Error:", e
    sys.exit ( 1 )


cm = doc.find_account ( u'Crédit Mutuel' )
tpe = doc.find_account ( u'TPE' )
vir_payee = doc.find_payee ( u'TPE' )
payee = doc.find_payee ( u'Boutique April' )
( vente_c, vente_s ) = doc.find_category_sub_category ( '7070' )
( don_c, don_s ) = doc.find_category_sub_category ( '7588' )
( cion_c, cion_s ) = doc.find_category_sub_category ( '6680' )

for line in sys.stdin:
    fields = line[:-1].split(";")
    t = tpe . create_transaction ( payee )
    if fields [ 0 ] [ :3 ] == 'DON':
        t.set_category ( don_c, don_s )
    else:
        t.set_category ( vente_c, vente_s )
    t.set_amount ( fields [ 2 ] )
    t.set_date  ( fields [ 1 ] )
    t.set_comment  ( 'Achat boutique (ref %s)' % fields [ 0 ] )

    cion_amount = Decimal(fields [ 2 ])*Decimal('0.009') + Decimal('0.10')
    cion = doc.get_transaction ( { 'date': fields [ 1 ],
                                   'account': cm,
                                   'payee': vir_payee,
                                   'category' : cion_c,
                                   'sub_category' : cion_s,
                                   'payment_mode': 19 } )
    if cion:
        cion . set_amount ( Decimal ( cion . get_amount ( ) ) - cion_amount )
    else:
        print "Could not find transaction for", fields [ 1 ]
        cion = cm.create_transaction ( vir_payee )
        cion.set_payment_mode ( 19 )
        cion.set_date ( fields [ 1 ] )
        cion.set_category ( cion_c, cion_s )
        cion.set_amount ( - cion_amount )
        cion.set_comment ( u'Frais bancaires pour TPE (saisie auto)' )

    virement = doc.find_transactions ( { 'date': fields [ 1 ],
                                         'account': tpe,
                                         'payee': vir_payee,
                                         'payment_mode': 31 } )
    if virement:
        virement = virement [ 0 ]
        virement . set_amount ( Decimal ( virement . get_amount ( ) ) - Decimal ( fields [ 2 ] ) )
        contra = virement . get_contra_transaction ( )
        contra . set_amount ( Decimal ( contra . get_amount ( ) ) + Decimal ( fields [ 2 ] ) )
    else:
        virement = tpe . create_transaction ( vir_payee ) 
        virement.set_payment_mode ( 31 )
        virement.set_amount ( - Decimal ( fields [ 2 ] ) )
        virement.set_date  ( fields [ 1 ] )
        virement.set_comment ( 'Encaissement journalier (saisie auto)' )

        contra = cm . create_transaction ( vir_payee ) 
        contra.set_payment_mode ( 19 )
        contra.set_amount ( Decimal ( fields [ 2 ] ) )
        contra.set_date  ( fields [ 1 ] )
        contra.set_comment ( 'Encaissement journalier (saisie auto)' )

        virement . link_to_transaction ( contra )

doc.serialize ( 'out2.gsb' )

sys.exit(0)
