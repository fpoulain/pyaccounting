# -*- coding: utf-8 -*-
# PyAccounting
# Copyright (C) 2011 Benjamin Drieu
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

from lxml import etree
from Payee import *
from XMLObject import *
from unac import unac


#------------------------------------------------------------------------------
class GrisbiPayee ( Payee, XMLObject ) :
    """Class to handle a Grisbi payee."""

    #----------------------------------------------------------------------
    def __init__ ( self, document, node, name = None ):
        XMLObject.__init__ ( self, document, node )
        Payee.__init__ ( self )

        self.node = node
        self.document = document
        if ( name ):
            self.set_name ( name )
            self.node_set ( 'Txt', '(null)' )
            self.node_set ( 'Search', '(null)' )

    #----------------------------------------------------------------------
    def __str__ ( self ) :
        return '<GrisbiPayee %s, %s>' % ( self.get_id ( ), self.get_name ( ) )

    #----------------------------------------------------------------------
    def get_id ( self ):
        if self.node is not None and self.node_get('Nb'):
            return self.node_get('Nb')

    #----------------------------------------------------------------------
    def set_id ( self, id ):
        if self.node is not None:
            return self.node_set('Nb', str ( id ) )

    #----------------------------------------------------------------------
    def get_name ( self ):
        if self.node is not None and self.node_get('Na'):
            return self.node_get('Na')

    #----------------------------------------------------------------------
    def set_name ( self, name ):
        if self.node is not None:
            self.node_set ( 'Na', name )

    #----------------------------------------------------------------------
    def get_unac_name ( self ):
        return self.unac_string ( self.get_name ( ) )

    #----------------------------------------------------------------------
    def get_transactions ( self, predicates = False ):
        return self.document.get_transactions_from_payee ( self, predicates )

    #----------------------------------------------------------------------
    def create_transaction ( self, date, amount, type_no ):
        return GrisbiTransaction ( self.document, None, payee_no, date, amount, type_no )

    #----------------------------------------------------------------------
    def get_scheduled ( self ):
        return self.document.get_scheduled_from_payee ( self.get_id ( ) )

    #----------------------------------------------------------------------
    def remove ( self ):
        self.document.remove_payee ( self )
