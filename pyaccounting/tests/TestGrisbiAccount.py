import sys
import unittest
from lxml import etree
sys.path.append('.')
sys.path.append('pyaccounting')
from GrisbiDocument import *
from GrisbiAccount import *

class TestGrisbiAccount(unittest.TestCase):
    _doc = None
    _account = None

    @classmethod
    def setUpClass(TestGrisbiAccount):
        print "LOADING"
        TestGrisbiAccount._doc = GrisbiDocument ( 'pyaccounting/tests/test.gsb' )
        TestGrisbiAccount._account = TestGrisbiAccount._doc.find_account ( 'Banque Populaire' )

    @classmethod
    def tearDownClass(TestGrisbiAccount):
        del ( TestGrisbiAccount._doc )

    def setUp(self):
        self._doc = TestGrisbiAccount._doc
        self._account = self._doc.find_account ( 'Banque Populaire' )

    def test00_find_account(self):
        self.assertEqual ( self._account.get_id(), '0' )

    def test01_id(self):
        self.assertEqual ( self._account.get_id(), '0' )

    def test10_find_by_id(self):
        account_by_id = self._doc.find_account_by_id ( 2 )
        self.assertEqual ( account_by_id.get_id(), '2' )

    def test10_name(self):
        self.assertEqual ( str(self._account), '<GrisbiAccount Banque Populaire>' )

    def test20_last_transaction(self):
        node = self._doc.create_node ( self._account._xpath_first ( '/Grisbi' ), 'Transaction' )
        node.set ( 'Nb', '999999' )
        self.assertEqual ( self._doc.get_last_transaction(), 999999 )

suite = unittest.TestLoader().loadTestsFromTestCase(TestGrisbiAccount)
unittest.TextTestRunner(verbosity=2).run(suite)
