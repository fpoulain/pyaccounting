# -*- coding: utf-8 -*-
import sys
import unittest
from lxml import etree
sys.path.append('..')
sys.path.append('pyaccounting')
from GrisbiDocument import *
from GrisbiAccount import *
from GrisbiPayee import *

class TestGrisbiPayee(unittest.TestCase):
    _doc = None
    _account = None

    @classmethod
    def setUpClass(TestGrisbiPayee):
        print "LOADING"
        TestGrisbiPayee._doc = GrisbiDocument ( 'pyaccounting/tests/test.gsb' )

    @classmethod
    def tearDownClass(TestGrisbiPayee):
        del ( TestGrisbiPayee._doc )

    def setUp(self):
        self._doc = TestGrisbiPayee._doc

    def test00_create_payee(self):
        self._payee = self._doc.create_payee ( 'Matt Murdock' )
        self.assertTrue ( self._payee.get_id ( ) > 0 )
        self.assertEqual ( self._payee.get_name ( ), 'Matt Murdock' )

    def test01_find_payee(self):
        self._doc.create_payee ( u'Athanagor Würlitzer' )
        payee = self._doc.find_payee ( 'Athanagor Wurlitzer' )
        self.assertTrue ( payee )
        self.assertEqual ( payee.get_name ( ), u'Athanagor Würlitzer' )


suite = unittest.TestLoader().loadTestsFromTestCase(TestGrisbiPayee)
unittest.TextTestRunner(verbosity=2).run(suite)
