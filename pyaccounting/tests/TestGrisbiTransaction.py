import sys
import unittest
from lxml import etree
sys.path.append('..')
sys.path.append('pyaccounting')
from GrisbiDocument import *
from GrisbiAccount import *
from GrisbiTransaction import *

class TestGrisbiTransaction(unittest.TestCase):
    _doc = None
    _account = None

    @classmethod
    def setUpClass(TestGrisbiTransaction):
        print "LOADING"
        TestGrisbiTransaction._doc = GrisbiDocument ( 'pyaccounting/tests/test.gsb' )
        TestGrisbiTransaction._account = TestGrisbiTransaction._doc.find_account ( 'Banque Populaire' )

    @classmethod
    def tearDownClass(TestGrisbiTransaction):
        del ( TestGrisbiTransaction._doc )

    def setUp(self):
        self._doc = TestGrisbiTransaction._doc
        self._account = self._doc.find_account ( 'Banque Populaire' )
        self._payee = self._doc.find_payee ( 'Benjamin Drieu' )
        self._transaction = self._account.create_transaction ( self._payee )

    def test00_create_transation(self):
        self.assertTrue ( self._transaction.get_id ( ) > 0 )
        self.assertEqual ( self._transaction.get_date ( ), date.today() )

    def test10_get_payee(self):
        payee = self._transaction.get_payee ( )
        self.assertTrue ( payee )
        self.assertEqual ( payee.get_id ( ), '35' )


suite = unittest.TestLoader().loadTestsFromTestCase(TestGrisbiTransaction)
unittest.TextTestRunner(verbosity=2).run(suite)
