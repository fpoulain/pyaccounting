# -*- coding: utf-8 -*-
# PyAccounting
# Copyright (C) 2011 Benjamin Drieu
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

from lxml import etree
from SubCategory import *
from XMLObject import *

from GrisbiCategory import *


#------------------------------------------------------------------------------
class GrisbiSubCategory ( SubCategory, XMLObject ) :
    """Class to handle a Grisbi category."""

    #----------------------------------------------------------------------
    def __init__ ( self, document, node, name = None ):
        XMLObject.__init__ ( self, document, node )
        SubCategory.__init__ ( self )

        self.node = node
        self.document = document
        if ( name ):
            self.set_name ( name )

    #----------------------------------------------------------------------
    def __str__ ( self ) :
        return '<GrisbiSubCategory %s, %s>' % ( self.get_id ( ), self.get_name ( ) )

    #----------------------------------------------------------------------
    def get_id ( self ):
        if self.node is not None and self.node_get('Nb'):
            return self.node_get('Nb')

    #----------------------------------------------------------------------
    def set_id ( self, id ):
        if self.node is not None:
            return self.node_set('Nb', str ( id ) )

    #----------------------------------------------------------------------
    def get_name ( self ):
        if self.node is not None and self.node_get('Na'):
            return self.node_get('Na')

    #----------------------------------------------------------------------
    def set_name ( self, name ):
        if self.node is not None:
            self.node_set ( 'Na', name )

    #----------------------------------------------------------------------
    def get_parent ( self ):
        return self.document.find_category_by_id ( self.node.get ( 'Nbc' ) )
